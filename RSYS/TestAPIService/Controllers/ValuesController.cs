﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace TestAPIService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        
        public ValuesController(NLog.ILogger logger, ICommandBus commandBus, IConfiguration configuration)
        {
            var Logger = logger;
            CommandBus = commandBus;
            var config = configuration;
            Logger.Info("asdfasfsdaf");
            SqlConnection sqlconnector = new SqlConnection(config.GetConnectionString("default"));
        }
        ICommandBus CommandBus { get; set; }
        // GET api/values public ActionResult<IEnumerable<string>> Get()
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            //var msg = new BaseMessages(MessageType.Info, "Hallo");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
