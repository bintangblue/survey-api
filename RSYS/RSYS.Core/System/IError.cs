﻿namespace System
{
    /// <summary>
    /// IError
    /// Interface Templates for ErrorMessages
    /// </summary>
    public interface IError
    {
        /// <summary>
        /// Hold The Last Exception 
        /// </summary>
        Exception Exception { get; set; }

        /// <summary>
        /// Numeric Error Code 
        /// </summary>
        long? ErrorCode { get; set; }
    }
}
