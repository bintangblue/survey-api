﻿/*
 **************************************************************
 * Author: Irfansjah
 * Email: irfansjah@gmail.com
 * Created: 07/14/2018
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 **************************************************************  
*/
using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class ExtensionUtility
    {
        #region Object Extension
        /// <summary>
        /// Check If obj value is null
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNull(this object obj)
        {
            return obj == null;
        }

        /// <summary>
        /// Check If obj value is not null
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNotNull(this object obj)
        {
            return obj != null;
        }

        public static void ThrowIfParameterNull<T>(this T parameter)
        {
            if (parameter.IsNull())
                throw new ArgumentNullException(
                        //"parameter",
                        string.Format("Parameters ({0}) not defined",
                        typeof(T).FullName));
        }
        #endregion

        #region String Extension

        /// <summary>
        /// Create New String Id extension
        /// </summary>
        /// <param name="data"></param>
        /// <param name="newid"></param>
        /// <returns></returns>
        public static string NewId(this string data, string newid = "")
        {
            if (!string.IsNullOrEmpty(data) && data.Trim().Length != 0) return data;
            data = string.IsNullOrEmpty(newid) ? StringUtility.CreateUniqueId : newid;
            return data;
        }

        /// <summary>
        /// Use string as a string format
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        /// <usage>
        ///     "this template {0} a {1}".StringFormat("is","sample")
        /// </usage>
        public static string StringFormat(this string obj, params object[] param)
        {
            return string.Format(obj, param);
        }

        public static string CreateMD5Hash(this string str)
        {
            return StringUtility.GetHashString(HashStringMode.MD5, str);
        }
        public static string CreateSHA1Hash(this string str)
        {
            return StringUtility.GetHashString(HashStringMode.SHA1, str);
        }
        public static string CreateSHA256Hash(this string str)
        {
            return StringUtility.GetHashString(HashStringMode.SHA256, str);
        }
        public static string CreateSHA384Hash(this string str)
        {
            return StringUtility.GetHashString(HashStringMode.SHA384, str);
        }
        public static string CreateSHA512Hash(this string str)
        {
            return StringUtility.GetHashString(HashStringMode.SHA512, str);
        }

        public static string FormatNumber(this int anumber, int length, string format = "{0}{1}", string repeatedChar = "0")
        {
            return StringUtility.MakeStrFormatedNumber(anumber, length, format, repeatedChar);
        }

        public static void SetDictionary(this System.Collections.IDictionary obj, object key, object value)
        {
            List<Object> keys = new List<object>();
            foreach (object item in obj)
            {

                if (keys.IndexOf(item) == -1)
                {
                    keys.Add(item);
                }
            }
            if (keys.IndexOf(key) > -1)
                obj[key] = value;
            else
                obj.Add(key, value);
        }

        public static void SetDictionary(this IDictionary<string, object> obj, string key, object value)
        {
            if (obj.ContainsKey(key))
                obj[key] = value;
            else
                obj.Add(key, value);
        }



        #endregion


        #region Reflection

        /// <summary>
        /// Set obj property value based on its property name
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="beforeExecute"></param>
        /// <param name="executed"></param>
        public static void SetPropertyValue(this object obj, string propertyName, object value,
            Action<object, object[]> beforeExecute = null,
            Action<object, object[]> executed = null)
        {
            if (obj.IsNull()) return;
            var propInfo = obj.GetType().GetProperty(propertyName);
            if (propInfo.IsNull()) return;
            try
            {
                var oldValue = propInfo.GetValue(obj);
                if (beforeExecute.IsNotNull())
                    // ReSharper disable once PossibleNullReferenceException
                    beforeExecute(obj, new[] { propertyName, value, oldValue });
                propInfo.SetValue(obj, value);
                if (executed.IsNotNull())
                    // ReSharper disable once PossibleNullReferenceException
                    executed(obj, new[] { propertyName, value, oldValue });
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Get obj property value
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static object GetPropertyValue(this object obj, string propertyName)
        {
            if (obj.IsNull()) return null;
            var propInfo = obj.GetType().GetProperty(propertyName);
            if (propInfo.IsNull()) return null;
            return propInfo.GetValue(obj);
        }

        /// <summary>
        /// Get obj property value return as T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static T GetPropertyValue<T>(this object obj, string propertyName)
        {
            var res = obj.GetPropertyValue(propertyName);
            return (T)res;
        }

        /// <summary>
        /// Check If obj has property propertyName
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool HasThisProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperties().Any(u => u.Name.Equals(propertyName));
        }

        /// <summary>
        /// Check If obj has Method methodName
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="methodName"></param>
        /// <returns></returns>
        public static bool HasThisMethod(this object obj, string methodName)
        {
            return obj.GetType().GetMethods().Any(u => u.Name.Equals(methodName));
        }

        /// <summary>
        /// Execute public Method
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="methodName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static object ExecuteMethod(this object obj, string methodName, params object[] parameters)
        {
            if (obj.IsNull()) return null;
            var mtd = obj.GetType().GetMethod(methodName);
            if (mtd.IsNotNull())
            {
                return mtd.Invoke(obj, parameters);
            }
            else return null;
        }

        /// <summary>
        /// Execute public Method
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="methodName"></param>
        /// <param name="executed"></param>
        /// <param name="parameters"></param>
        /// <param name="beforeExecute"></param>
        /// <returns></returns>
        public static object ExecuteMethod(this object obj, string methodName,
            Action<object, object[]> beforeExecute,
            Action<object, object[]> executed, params object[] parameters)
        {
            if (obj.IsNull()) return null;
            var mtd = obj.GetType().GetMethod(methodName);
            if (mtd.IsNotNull())
            {
                Exception errorbuf = null;
                object result = null;
                if (beforeExecute.IsNotNull())
                    beforeExecute(obj, parameters);
                try
                {
                    result = mtd.Invoke(obj, parameters);
                }
                catch (Exception err)
                {
                    errorbuf = err;
                }
                if (executed.IsNotNull())
                    executed(obj, parameters);
                if (errorbuf.IsNull())
                    return result;
                // ReSharper disable once PossibleNullReferenceException
                throw errorbuf;
            }
            else return null;
        }

        #endregion

        //public static bool IsResultSuccess<T>(this ICommandResult<T> result) where T : class
        //{
        //    // ReSharper disable once SuspiciousTypeConversion.Global
        //    return result.Success && (result is IErrorResultData<T>);
        //}

        //public static bool IsResultSuccess<T>(this IResultData<T> result) where T : class
        //{
        //    return !(result is IErrorResultData<T>);
        //}
    }
}
